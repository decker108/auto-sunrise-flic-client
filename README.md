# Flic client for Auto Sunrise

This project requires an Auto Sunrise setup, a Flic button and a Raspberry Pi 3 with BLE enabled (this might require some firmware upgrades).

## Setup

1. systemctl stop bluetooth
2. Clone https://github.com/50ButtonsEach/fliclib-linux-hci
3. ./flicd -f flic.sqlite3 -d
4. pip install -r requirements.txt

See https://github.com/50ButtonsEach/fliclib-linux-hci for more info about Flic setup

## Running

1. sudo systemctl stop bluetooth
2. flicd -f flic.sqlite3 -d //look for flicd in home dir
3. python3 aioflic_client.py &

