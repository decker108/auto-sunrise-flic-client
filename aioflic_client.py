#!/bin/bash python3

import asyncio
from aioflic import *
import datetime
import logging
import logging.handlers
import requests

my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address = '/dev/log')
my_logger.addHandler(handler)

def open_or_close(clickType):
    if clickType == 'ClickType.ButtonSingleClick':
        my_logger.debug('[auto-sunrise-flic-client] Opening curtain via flic')
        requests.post('http://smartfanpi:8080/open')
    elif clickType == 'ClickType.ButtonDoubleClick':
        my_logger.debug('[auto-sunrise-flic-client] Closing curtain via flic')
        requests.post('http://smartfanpi:8080/close')
    else:
        my_logger.debug('[auto-sunrise-flic-client] Got unknown event: ' + clickType)

def got_button(bd_addr):
    #my_logger.debug("Simple or Double or hold {} {} {}".format(channel.bd_addr,str(click_type),time_diff))
    cc = ButtonConnectionChannel(bd_addr)
    cc.on_button_single_or_double_click_or_hold = \
        lambda channel, click_type, was_queued, time_diff: \
            open_or_close(str(click_type))
    cc.on_connection_status_changed = \
        lambda channel, connection_status, disconnect_reason: \
            my_logger.debug(channel.bd_addr + " " + str(connection_status) + (" " + str(disconnect_reason) if connection_status == ConnectionStatus.Disconnected else ""))
    client.add_connection_channel(cc)

def got_info(items):
    my_logger.debug(items)
    for bd_addr in items["bd_addr_of_verified_buttons"]:
        got_button(bd_addr)
    scan()
        
def on_found_private_button(scan_wizard):
    my_logger.debug("Found a private button. Please hold it down for 7 seconds to make it public.")

def on_found_public_button(scan_wizard, bd_addr, name):
    my_logger.debug("Found public button " + bd_addr + " (" + name + "), now connecting...")

def on_button_connected(scan_wizard, bd_addr, name):
    my_logger.debug("The button was connected, now verifying...")
    got_button(bd_addr)

def on_completed(scan_wizard, result, bd_addr, name):
    my_logger.debug("Scan wizard completed with result " + str(result) + ".")
    if result == ScanWizardResult.WizardSuccess:
        my_logger.debug("Your button is now ready. The bd addr is " + bd_addr + ".")


def scan():
    my_logger.debug("Starting the scan")
    mywiz=ScanWizard()
    mywiz.on_found_private_button = on_found_private_button
    mywiz.on_found_public_button = on_found_public_button
    mywiz.on_button_connected = on_button_connected
    mywiz.on_completed = on_completed
    client.add_scan_wizard(mywiz)

            
FlicClient.on_get_info=got_info
loop = asyncio.get_event_loop()  
try:
    coro = loop.create_connection(lambda: FlicClient( loop),
                            'localhost', 5551)
    conn,client=loop.run_until_complete(coro)
    client.on_get_info=got_info
    client.get_info()
    loop.run_forever()
except  KeyboardInterrupt:
    my_logger.debug("Exiting at user's request")
finally:
    client.close()
    loop.close()   